package org.httpbin.app.Events

import androidx.navigation.NavDirections

class NavigationEvent(val action:NavDirections)
