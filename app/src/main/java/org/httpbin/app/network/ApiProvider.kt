package org.httpbin.app.network

import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.httpbin.app.utils.ConstantProvider.HTTP_PROTOCOL
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiProvider<T>(
    private val api: Class<T>,
    private val baseUrl: String?

) : javax.inject.Provider<T> {

    private val retrofit by lazy {
        val okhttpClientBuilder = OkHttpClient.Builder()

            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okhttpClientBuilder.addInterceptor(httpLoggingInterceptor)
            okhttpClientBuilder.addNetworkInterceptor(StethoInterceptor())
                .retryOnConnectionFailure(true)

        Retrofit.Builder()
            .client(okhttpClientBuilder.build())
            .baseUrl("$HTTP_PROTOCOL$baseUrl")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    override fun get(): T = retrofit.create(api)
}





