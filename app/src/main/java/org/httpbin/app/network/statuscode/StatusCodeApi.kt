package org.httpbin.app.network.statuscode

import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Path

interface StatusCodeApi {

    @POST("status/{codes}")
    fun postStatusCodes(
        @Path("codes") statusCode: Int,
    ): Call<Void>

}