package org.httpbin.app.network.statuscode

import org.httpbin.app.network.ApiProvider
import org.httpbin.app.utils.ConstantProvider
import org.httpbin.app.utils.StatusCodeListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StatusCodeService {

    private val statusCodeApi =
        ApiProvider(api = StatusCodeApi::class.java, baseUrl = ConstantProvider.BASE_URL).get()

    fun postStatusCode(statusCode:Int,statusCodeListener: StatusCodeListener){
        statusCodeListener.executing()
        var responseCodes: List<Int>
        statusCodeApi.postStatusCodes(statusCode).enqueue(object :Callback<Void>{
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful){
                    statusCodeListener.onSucceed()
                }else{
                    statusCodeListener.onError("Unknown")
                }
            }
            override fun onFailure(call: Call<Void>, t: Throwable) {
                statusCodeListener.onError(t.message.toString())
            }

        })
    }

}