package org.httpbin.app.network.user

import org.httpbin.app.network.dto.UserDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface UserApi {

    @GET("basic-auth/{username}/{password}")
    fun authenticate(
        @Header("Authorization") authKey: String,
        @Path("username") username: String,
        @Path("password") password: String
    ): Call<UserDto>
}