package org.httpbin.app.network.user

import android.util.Base64
import org.httpbin.app.model.User
import org.httpbin.app.network.ApiProvider
import org.httpbin.app.network.dto.UserDto
import org.httpbin.app.utils.ConstantProvider.BASE_URL
import org.httpbin.app.utils.ConstantProvider.BASIC_KEY
import org.httpbin.app.utils.LoginListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class UserAuthenticator {

    private val userApi = ApiProvider(api = UserApi::class.java, baseUrl = BASE_URL).get()

    fun authenticateUser(user: User, loginListener: LoginListener) {
        val authKey = createAuthPath(user)
        loginListener.onLogging()
        userApi.authenticate(authKey, user.username, user.password)
            .enqueue(object : Callback<UserDto> {
                override fun onResponse(call: Call<UserDto>, response: Response<UserDto>) {
                    if (response.isSuccessful && response.body()?.authenticated == true) {
                        loginListener.onSucceed()
                    } else {
                        val message = if (response.message()
                                .isNullOrEmpty()
                        ) "Login failed" else response.message()
                        loginListener.onFailed(message, response.code())
                    }
                }

                override fun onFailure(call: Call<UserDto>, t: Throwable) {
                    val message = if (t.message.isNullOrEmpty()) "Unknown error!" else t.message!!
                    loginListener.onFailed(message, null)
                }

            })
    }

    private fun createAuthPath(user: User): String {
        val credentials = "${user.username}:${user.password}"
        return "$BASIC_KEY ${Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)}"
    }
}