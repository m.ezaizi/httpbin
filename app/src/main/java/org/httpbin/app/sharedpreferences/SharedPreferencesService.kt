package org.httpbin.app.sharedpreferences

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

private const val USERNAME = "USERNAME"
private const val COUNTER_KEY = "COUNTER"

class SharedPreferencesService(application: Application) {

    private val sharedPreferences = application.getSharedPreferences("Preferences", Context.MODE_PRIVATE)

    fun saveUser(username:String) = sharedPreferences.edit().putString(USERNAME,username).apply()

    fun getUser() = sharedPreferences.getString(USERNAME,"")
    fun deleteUser(username:String) = sharedPreferences.edit().remove(USERNAME).apply()

    fun getCounter() = sharedPreferences.getInt(COUNTER_KEY,0)
    fun saveCounter(counter:Int) = sharedPreferences.edit().putInt(COUNTER_KEY,counter).apply()
    fun deleteCounter() = sharedPreferences.edit().remove(COUNTER_KEY).apply()

    fun resetPreferences() = sharedPreferences.edit().clear().apply()

}