package org.httpbin.app.ui.dashboard

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import org.httpbin.app.R
import org.httpbin.app.databinding.FragmentDashboardBinding
import org.httpbin.app.ui.main.MainActivity

class DashboardFragment : Fragment() {

    private lateinit var fragmentDashboardBinding: FragmentDashboardBinding
    private val dashboardViewModel: DashboardViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        fragmentDashboardBinding = FragmentDashboardBinding.bind(root).apply {
            model = dashboardViewModel
        }
        fragmentDashboardBinding.lifecycleOwner = this
        setHasOptionsMenu(true)
        return fragmentDashboardBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)?.supportActionBar?.title =
            getString(R.string.hello, dashboardViewModel.username)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.add("Logout").setIcon(R.drawable.ic_logout).setOnMenuItemClickListener {
            dashboardViewModel.logout()
            true
        }
        menu.add("Reset counter").setIcon(R.drawable.ic_logout).setOnMenuItemClickListener {
            dashboardViewModel.resetCounter()
            true
        }
    }

}