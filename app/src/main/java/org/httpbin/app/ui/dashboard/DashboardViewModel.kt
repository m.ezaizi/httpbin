package org.httpbin.app.ui.dashboard

import org.httpbin.app.Events.LogoutEvent
import android.app.Application
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import org.greenrobot.eventbus.EventBus
import org.httpbin.app.network.statuscode.StatusCodeService
import org.httpbin.app.sharedpreferences.SharedPreferencesService
import org.httpbin.app.utils.StatusCodeListener

class DashboardViewModel(application: Application) : AndroidViewModel(application) {


    val preferences: SharedPreferencesService by lazy {
        SharedPreferencesService(application)
    }

    val statusCodeService: StatusCodeService by lazy {
        StatusCodeService()
    }

    val statusCode = MutableLiveData("")

    val message = MutableLiveData("")
    val counter = MutableLiveData(preferences.getCounter().toString())

    val username = preferences.getUser()

    val statusCodeTextWatcher = object : TextWatcher {

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(p0: Editable?) {
            statusCode.value = p0.toString()
        }
    }

    val statusCodeListener = object : StatusCodeListener {
        override fun executing() {
        }

        override fun onSucceed() {
            val counter = preferences.getCounter() + 1
            preferences.saveCounter(counter)
            this@DashboardViewModel.counter.value = counter.toString()
            message.value = "Succeeded!"
        }

        override fun onError(message: String) {
            this@DashboardViewModel.message.value = "Error: $message"
        }
    }

    fun postStatusCode() {
        message.value = ""
        if (statusCode.value.isNullOrEmpty()) {
            message.value = "Status code input cannot be empty!"
        } else {
            try {
                statusCodeService.postStatusCode(statusCode.value!!.toInt(), statusCodeListener)
            } catch (e: Exception) {
                message.value = "Number format error!"
            }
        }
    }

    fun resetCounter(){
        preferences.deleteCounter()
        message.value = ""
        counter.value = "0"
    }

    fun logout() {
        preferences.resetPreferences()
        EventBus.getDefault().postSticky(LogoutEvent())
    }
}