package org.httpbin.app.ui.login

import android.app.Application
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import org.greenrobot.eventbus.EventBus
import org.httpbin.app.model.User
import org.httpbin.app.network.user.UserAuthenticator
import org.httpbin.app.sharedpreferences.SharedPreferencesService
import org.httpbin.app.Events.NavigationEvent
import org.httpbin.app.utils.LoginListener

class LoginViewModel(application: Application) : AndroidViewModel(application) {


    private val loginAuthenticator: UserAuthenticator by lazy {
        UserAuthenticator()
    }

    val preferences: SharedPreferencesService by lazy {
        SharedPreferencesService(application)
    }

    val username = MutableLiveData("")
    val password = MutableLiveData("")
    val statusMessage = MutableLiveData("")

    fun onLoginClick() {
        val user = User(username.value!!, password.value!!)
        loginAuthenticator.authenticateUser(user, loginListener)
    }

    val usernameTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(p0: Editable?) {
            username.value = p0.toString()
        }

    }

    val passwordTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(p0: Editable?) {
            password.value = p0.toString()
        }

    }
    val loginListener = object : LoginListener {

        override fun onLogging() {
            statusMessage.value = "Logging.."
        }

        override fun onSucceed() {
            preferences.saveUser(username.value!!)
            EventBus.getDefault().post(
                NavigationEvent(
                    LoginFragmentDirections.actionLoginToDashboard()
                )
            )
        }

        override fun onFailed(errorMessage: String, statusCode: Int?) {
            statusMessage.value = errorMessage
        }
    }
}