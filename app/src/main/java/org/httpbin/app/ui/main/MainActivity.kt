package org.httpbin.app.ui.main

import org.httpbin.app.Events.LogoutEvent
import org.httpbin.app.Events.NavigationEvent
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.httpbin.app.R
import org.httpbin.app.sharedpreferences.SharedPreferencesService
import org.httpbin.app.ui.dashboard.DashboardFragmentDirections
import org.httpbin.app.ui.login.LoginFragmentDirections

class MainActivity : AppCompatActivity() {

    private lateinit var navGraphController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navGraphController = Navigation.findNavController(this, R.id.nav_host_fragment)
        navigateToDashboardIfUserSaved()
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    private fun navigateToDashboardIfUserSaved() {
        val username = SharedPreferencesService(this.application).getUser()
        if (!username.isNullOrEmpty()) {
            navigateToDashboard()
        }
    }


    private fun navigateToDashboard() =
        findNavController(R.id.nav_host_fragment).navigate(
            LoginFragmentDirections.actionLoginToDashboard()
        )

    @SuppressLint("RestrictedApi")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun navigate(navigationEvent: NavigationEvent) =
        navGraphController.navigate(navigationEvent.action)


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun logout(logoutEvent: LogoutEvent) {
     supportActionBar?.title = getString(R.string.app_name)
        findNavController(R.id.nav_host_fragment).navigate(
            DashboardFragmentDirections.actionDashboardToLogin()
        )
    }

}