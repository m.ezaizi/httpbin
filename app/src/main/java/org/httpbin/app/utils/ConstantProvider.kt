package org.httpbin.app.utils

object ConstantProvider {
    const val BASIC_KEY = "Basic"
    const val HTTP_PROTOCOL = "https:"
    const val BASE_URL = "httpbin.org/"
}