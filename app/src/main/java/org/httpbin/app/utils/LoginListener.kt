package org.httpbin.app.utils

interface LoginListener {

    fun onLogging()
    fun onSucceed()
    fun onFailed(errorMessage: String, statusCode: Int?)
}