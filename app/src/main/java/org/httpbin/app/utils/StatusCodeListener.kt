package org.httpbin.app.utils

interface StatusCodeListener {

    fun executing()
    fun onSucceed()
    fun onError(message:String)
}